# Wstęp

1. Monitorowanie stanu zdrowia aplikacji
   Minecraft Java Edition (po kupieniu przez MS powstała wersja Bedrock napisana w C++).

2. Co to są metryki wydajności?
   Task Manager, htop i inne

3. Prometheus - omówienie

   Prometheus to narzędzie open source stworzone przez SoundCloud do kolekcjonowania metryk i generowania alertów.

   Składa się ono z czterech głównych komponentów:

   - głównej aplikacji zbierającej i magazynującej metryki (w wewnętrznej bazie danych typu time-series) oraz udostępniającej te metryki przy zapytaniach,
   - eksporterami, które zbierają dane z różnych źródeł i konwertują je do metryk, które mogą zostać zebrane przez Prometheusa; eksportery są dedykowane konkretnym rozwiązaniom sprzętowym lub aplikacjom,
   - AlertManager, do zarządzania alarmami budowanymi na podstawie metryk; dostarczany razem z główną aplikacją,
   - biblioteki klienckie do udostępniania metryk z tworzonych aplikacji.

   Prometheus z założenia magazynuje wszystkie metryki w postaci serii czasowych - list wartości należących do konkretnej metryki ze stemplami czasowymi.

4. Biedastack do monitorowania aplikacji:

   - Grafana - wizualizacja danych (będzie pokazany na koniec),
   - Loki - zbieranie logów (nie będzie omawiany),
   - Prometheus - metryki wydajności (prezentacja techniczna).

5. Prometheus - typy metryk

   - counter (licznik) - prosty licznik, który może być inkrementowany,
   - gauge (wskaźnik) - pojedyncza wartość liczbowa mogąca dowolnie się zmieniać,
   - histogram - zbiór liczników; próbki są zbierane w konfigurowalny buckety (np. liczba requestów zajmująca <100ms, <200ms, <300ms, <400ms, <500ms, 500ms+); wraz z tą metryką udostępniana jest metryka informująca o całkowitej liczbie próbek,
   - podsumowanie - metryka podobna do histogramu, jednak zamiast liczników zwraca ono kwantyle (wyniki funkcje obliczeniowych działających w zadanych przedziałach czasowych).

   Na podstawie histogramów też można otrzymywać kwantyle. Jedyna różnica polega na tym, po której stronie wykonywane są obliczenia: klienta dla podsumowań, serwera dla histogramów.

6. Biblioteka prometheus-net: https://github.com/prometheus-net/prometheus-net

# Prezentacje techniczna

## Prometheus - metryki wydajności

### Podstawowa konfiguracja

1. Dodanie paczek `prometheus-net` i `prometheus-net.AspNetCore`

2. Włączenie metryk

   ```csharp
   public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
   {
   	...
   	app.UseRouting();
       // Expose HTTP metrics to Prometheus middleware
   	app.UseHttpMetrics();
       ...
   	
   	app.UseEndpoints(endpoints =>
   	{
   		endpoints.MapControllers();
           ...
   		// Publish "/metrics" endpoint
   		endpoints.MapMetrics();
       });
   }
   ```

3. Wyświetlenie metryk pod adresem http://localhost:5000/metrics

### Podpięcie Prometheusa

1. Demo konfiguracji Prometheusa
2. Odpalenie `docker-compose up`
3. Otworzenie dashboardu Prometheusa (http://localhost:9090/targets).
4. Dodanie targetu do konfiguracji.
5. Odświeżenie konfiguracji `docker-compose kill -s SIGHUP prometheus` lub wysyłając POST na adres http://localhost/-/reload po włączeniu opcji `--web.enable-lifecycle`.

### Zapytania do Prometheusa

1. Wygenerowanie ruchu na endpointach.
2. Użycie PromQL
   - _Liczba requestów do aplikacji_: `http_requests_received_total{job="lottoapi"}`
   - _Liczba błędów w kontrolerze_: `http_requests_received_total{controller="Lotto", code=~"500|501|502|503", job="lottoapi"}`
   - _Suma wszystkich pomyślnych requestów_: `sum(http_requests_received_total{code="200", job="lottoapi"})`
   - _Pamięć zaalokowana dla procesu_ (w MB): `dotnet_total_memory_bytes / 1024 / 1024`
   - _Prędkość alokacji pamięci (w KB/s)_: `increase(dotnet_total_memory_bytes[5m]) / 1024`
   - _Liczba wątków w procesie_: `process_num_threads`

### Własna metryka

1. Zdefiniowanie metryki w kontrolerze.

   ```csharp
   public class LottoController : ControllerBase
   {
   	...
   	// Declare new metrics
   	private static readonly Histogram _longRequestDuration = Metrics.CreateHistogram("lottoapi_longrequest_duration_seconds", "Histogram of LongRequest call processing durations.");
   	private static readonly Counter _evenSecondsDurationCount = Metrics.CreateCounter("lottoapi_longrequest_even_duration_seconds", "Number of times LongRequest took even seconds.");
   	private static readonly Counter _unevenSecondsDurationCount = Metrics.CreateCounter("lottoapi_longrequest_uneven_duration_seconds", "Number of times LongRequest took uneven seconds.");
   	...
   }
   ```

2. Wykorzystanie metryki w metodzie kontrolera.

   ```csharp
   public class LottoController : ControllerBase
   {
   	...
   	[HttpGet("luckynumber")]
   	public IActionResult GetLuckyNumber()
   	{
   		...
   		// Update counter metrics by calling their's Inc method
   		if (number % 2 == 0)
   			_evenLuckyNumberCount.Inc();
   		else
   			_unevenLuckyNumberCount.Inc();
           ...
       }
       
       ...
   	
   	[HttpGet("longrequest")]
   	public async Task<IActionResult> LongRequest()
   	{
   		await Task.Run(() =>
   		{
   			// Measure time for histogram metric by wrapping operation in using statement
   			using (_longRequestDuration.NewTimer())
   			{
   				Random rng = new Random();
   
   				Thread.Sleep(rng.Next(10) * 1000);
   			}
   		});
           ...
   	}    
   	...
   }
   ```

### Grafana - przykładowy dashboard

1. Podpięcie Prometheusa jako źródła do Grafany (http://prometheus:9090).
2. Zaimportowanie dashboardu z YAML'a.
3. Prezentacja dodawania nowych paneli.

## Loki - structured logging


# Podsumowanie

- Loki
  https://github.com/serilog-contrib/serilog-sinks-grafana-loki
  https://www.davidguida.net/asp-net-core-structured-logging-part-3-the-code-finally/

- OpenTelemetry Prometheus exporter

  https://docs.microsoft.com/en-us/dotnet/core/diagnostics/metrics-collection#view-metrics-in-grafana-with-opentelemetry-and-prometheus

  > [OpenTelemetry](https://opentelemetry.io/) is a vendor-neutral open- source project supported by the [Cloud Native Computing Foundation](https://www.cncf.io/) that aims to standardize generating and collecting telemetry for cloud-native software. The built-in platform metric APIs are designed to be compatible with this standard to make integration straightforward for any .NET developers that wish to use it. At the time of writing, support for OpenTelemetry metrics is relatively new, but [Azure Monitor](https://docs.microsoft.com/en-us/azure/azure-monitor/app/opentelemetry-overview) and many major APM vendors have endorsed it and have integration plans underway.

- Możliwość zbierania metryk Prom przez Azure Monitor
  https://azure.microsoft.com/pl-pl/blog/azure-monitor-for-containers-with-prometheus-now-in-preview/
  https://docs.microsoft.com/en-us/azure/azure-monitor/containers/container-insights-prometheus-integration
  https://docs.microsoft.com/en-us/samples/azure-samples/dotnetapp-azure-prometheus/dotnet-azure-prometheus/

- Prometheus - zapytania
  https://prometheus.io/docs/prometheus/latest/querying/basics/
- ASP.NET Health Checks
  https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks
  https://github.com/prometheus-net/prometheus-net#aspnet-core-health-check-status-metrics
- Horizontal Pod Autoscaling
  https://dzone.com/articles/prometheus-metrics-based-autoscaling-in-kubernetes
  https://towardsdatascience.com/kubernetes-hpa-with-custom-metrics-from-prometheus-9ffc201991e