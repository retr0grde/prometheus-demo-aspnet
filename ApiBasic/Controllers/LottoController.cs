using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApmDemo.ApiBasic.Controllers
{
	[ApiController]
    [Route("[controller]")]
    public class LottoController : ControllerBase
    {
        private readonly ILogger<LottoController> _logger;

        public LottoController(ILogger<LottoController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            Random rng = new Random();
            int[] numbers = new int[6];

            for (int i = 0; i < numbers.Length;)
            {
                int newNumber = rng.Next(1, 49);

                if (!numbers.Contains(newNumber))
                    numbers[i++] = newNumber;
            }

            return Ok(numbers);
        }

        [HttpGet("badendpoint")]
        public IActionResult BadEndpoint()
        {
            Random rng = new Random();
            int number = rng.Next(1, 49);

            if (number % 2 == 0)
                return Ok(number);
            else
                return StatusCode(500, "No number for you, dumbass.");
        }

        [HttpGet("luckynumber")]
        public IActionResult GetLuckyNumber()
        {
            Random rng = new Random();

            return Ok(rng.Next(1, 49));
        }

        [HttpGet("longrequest")]
        public async Task<IActionResult> LongRequest()
        {
            await Task.Run(() => Thread.Sleep(10000));

            return Ok();
        }
    }
}
