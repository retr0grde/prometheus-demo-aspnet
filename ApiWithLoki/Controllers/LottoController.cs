using System;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApmDemo.ApiWithLoki.Controllers
{
	[ApiController]
    [Route("[controller]")]
    public class LottoController : ControllerBase
    {
        private readonly ILogger<LottoController> _logger;
		private readonly int[] _lottoNumbers;

        public LottoController(ILogger<LottoController> logger)
        {
            _logger = logger;

			Random rng = new Random();
			_lottoNumbers = new int[6];

			for (int i = 0; i < _lottoNumbers.Length;)
			{
				int newNumber = rng.Next(1, 49);

				if (!_lottoNumbers.Contains(newNumber))
					_lottoNumbers[i++] = newNumber;
			}
		}

        [HttpGet]
        public IActionResult SampleLogging()
        {
			_logger.LogInformation("Returning lotto numbers by endpoint...");

            return Ok(_lottoNumbers);
        }

		[HttpGet("sampleerror")]
		public IActionResult ErrorLogging()
		{
			int numberToReturn = -1;
			int indexToGet = _lottoNumbers.Length + 1;

			try
			{
				numberToReturn = _lottoNumbers[indexToGet];
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "An error occured while getting lotto number.");
				return StatusCode(500, "Internal server error");
			}

			return Ok(numberToReturn);
		}

		[HttpGet("interpolated")]
		public IActionResult LoggingUsingInterpolatedString()
		{
			int numberToReturn = -1;
			int indexToGet = _lottoNumbers.Length + 1;

			try
			{
				numberToReturn = _lottoNumbers[indexToGet];
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, $"An error occured while getting lotto number. Tried to get number no. {indexToGet} from {_lottoNumbers.Length}-element array");
				return StatusCode(500, "Internal server error");
			}

			return Ok(numberToReturn);
		}

		[HttpGet("withparams")]
		public IActionResult LoggingWithParams()
		{
			int numberToReturn = -1;
			int indexToGet = _lottoNumbers.Length + 1;

			try
			{
				numberToReturn = _lottoNumbers[indexToGet];
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "An error occured while getting lotto number. Tried to get number no. {arrayIndex} from {arrayLength}-element array", indexToGet, _lottoNumbers.Length);
				return StatusCode(500, "Internal server error");
			}

			return Ok(numberToReturn);
		}
	}
}
