using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Prometheus;

namespace ApmDemo.ApiWithProm.Controllers
{
	[ApiController]
    [Route("[controller]")]
    public class LottoController : ControllerBase
    {
        private readonly ILogger<LottoController> _logger;

        #region Metrics
        // Declare new metrics
        private static readonly Histogram _longRequestDuration = Metrics.CreateHistogram("lottoapi_longrequest_duration_seconds", "Histogram of LongRequest call processing durations.");
        private static readonly Counter _evenLuckyNumberCount = Metrics.CreateCounter("lottoapi_luckynumber_even_count", "Number of times lucky number was even.");
        private static readonly Counter _unevenLuckyNumberCount = Metrics.CreateCounter("lottoapi_luckynumber_uneven_count", "Number of times lucky number was uneven.");
        #endregion

        public LottoController(ILogger<LottoController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            Random rng = new Random();
            int[] numbers = new int[6];

            for (int i = 0; i < numbers.Length;)
            {
                int newNumber = rng.Next(1, 49);

                if (!numbers.Contains(newNumber))
                    numbers[i++] = newNumber;
            }

            return Ok(numbers);
        }

        [HttpGet("badendpoint")]
        public IActionResult BadEndpoint()
        {
            Random rng = new Random();
            int number = rng.Next(1, 49);

            if (number % 2 == 0)
                return Ok(number);
            else
                return StatusCode(500, "No number for you, dumbass.");
        }

        [HttpGet("luckynumber")]
        public IActionResult GetLuckyNumber()
        {
            Random rng = new Random();
            int number = rng.Next(1, 49);

            // Update counter metrics by calling their's Inc method
            if (number % 2 == 0)
                _evenLuckyNumberCount.Inc();
            else
                _unevenLuckyNumberCount.Inc();

            return Ok(number);
        }

        [HttpGet("longrequest")]
        public async Task<IActionResult> LongRequest()
        {
            await Task.Run(() =>
            {
            // Measure time for histogram metric by wrapping operation in using statement
            using (_longRequestDuration.NewTimer())
                {
                    Random rng = new Random();

                    Thread.Sleep(rng.Next(10) * 1000);
                }
            });

            return Ok();
        }
    }
}
